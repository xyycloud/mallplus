package com.zscat.mallplus.ums.mapper;

import com.zscat.mallplus.ums.entity.UmsRewardLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zscat
 * @since 2019-07-02
 */
public interface UmsRewardLogMapper extends BaseMapper<UmsRewardLog> {

}
